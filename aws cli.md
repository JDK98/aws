AWS CLI
---

AWS CLI est l'interface d'administration en ligne de commande de AWS <br>

[AWS CLI DOC](https://docs.aws.amazon.com/cli/latest/reference/) <br>

## Créer une instance EC2 en CLI <br>

**1. Créer la clé SSH** <br>

```sh
aws ec2 create-key-pair --key-name <keyPairName> --query 'KeyMaterial' --region <region>
``` 

**2. Créer la règle de sécurité** <br>

```sh
aws ec2 create-security-group --group-name <sgName> --description "My security group" --region <region>
```

**3. Créer une instance EC2** <br>

```sh
aws ec2 run-instances --image-id  <imageName> --count 1 --instance-type <instanceType> --key-name <keyPairName> --security-groups <sgName> --region <region>
```

**4. Supprimer une instance** <br>

```sh
aws ec2 terminate-instances --instance-ids <instanceId> --region <region>
``` 

