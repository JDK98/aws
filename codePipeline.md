AWS CODE PIPELINE
---

👉‍ AWS CodePipeline vous aide à modéliser, visualiser et automatiser les étapes nécessaires pour livrer en continu la version de votre logicie. <br> <br>

👉‍ À l'aide d'AWS CodePipeline, vous pouvez créer et configurer différentes étapes d'un processus de publication de logiciel. Il vous aide à automatiser la livraison de logiciels ayant une version fréquente. <br> <br>

👉‍ AWS CodePipeline peut vous aider à effectuer diverses tâches, telles que <br> <br>

    Automatisation du processus de publication de la livraison de logiciels

    Afficher les détails de l'historique du pipeline

    Personnalisez les étapes et l'action

    Choisissez la source, créez et déployez l'étape.

    Ajoutez l'approbation manuelle pour le processus de déploiement.

    Intégration avec divers services AWS.


![Jenkins Icon for Jenkins Module](/cdd22.png "CodePipeline")


👉‍ **Source** : Un endroit où le code source avec ses révisions est stocké. Il peut s'agir d'un compartiment Amazon S3, d'un référentiel CodeCommit ou de référentiels basés sur Git tels que GitHub et Bitbucket. <br> <br>

👉‍ **Build**: Dans cette phase, le fichier buildspec.yml joue un rôle très important. C'est la phase où le logiciel est testé et construit (CodeBuild).  <br> <br>

👉‍ **Staging**: Un endroit où le code est déployé dans un environnement de test ou également appelé environnement de pré-production. Vous utilisez cette phase pour tester le logiciel avant de le mettre en ligne pour les utilisateurs publics. Il s'agit d'une étape facultative si l'application est bien testée avant l'upload vers la source. <br> <br>

👉‍ **Manual approval**: Cette phase aide les développeurs à obtenir l'approbation du code, déployé et bien testé dans l'environnement de test. Il utilise SNS pour obtenir les approbations par e-mail. C'est une étape facultative. <br> <br>

👉‍ **Deploy**: La dernière phase pour la livraison du logiciel. les applications peuvent être déployées sur des instances Amazon EC2 à l'aide de CodeDeploy, Elastic Beanstalk ou AWS OpsWorks Stacks. <br> <br>
