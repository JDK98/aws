    # Create a VPC with IpAddresses 10.10.0.0/16, a NAT gateway, a public subnet, PRIVATE_WITH_EGRESS subnet and a RDS subnet

        vpc = ec2.Vpc(self, "MyVpc",
            ip_addresses = ec2.IpAddresses.cidr("10.10.0.0/16"),
            nat_gateways = 1,
            subnet_configuration = [
                ec2.SubnetConfiguration(name = "PUBLIC", subnet_type = ec2.SubnetType.PUBLIC, cidr_mask = 24),
                ec2.SubnetConfiguration(name = "PRIVATE_WITH_EGRESS", subnet_type = ec2.SubnetType.PRIVATE_WITH_EGRESS, cidr_mask = 24),
                ec2.SubnetConfiguration(name = "RDS", subnet_type = ec2.SubnetType.PRIVATE_ISOLATED, cidr_mask = 24)
            ]
        )
        
    # Create a security group for the load balancer
        security_group = ec2.SecurityGroup(self, "SecurityGroup",
            vpc = vpc,
            description = "Allow HTTP",
            allow_all_outbound = True
        )
    
    # Create a security group for the RDS instance
        rds_security_group = ec2.SecurityGroup(self, "RDSSecurityGroup",
            vpc = vpc,
            description = "Allow access to RDS",
            allow_all_outbound = True
        )
            
        # Create a security group for the EC2 instance
        ec2_security_group = ec2.SecurityGroup(self, "EC2SecurityGroup",
            vpc = vpc,
            description = "Allow access to RDS",
            allow_all_outbound = True
        )
            
        # add ingress rules for the load balancer to allow all traffic
        security_group.add_ingress_rule(ec2.Peer.any_ipv4(), ec2.Port.tcp(80))
        
        # add ingress rule for the EC2 instance to allow 8443 traffic from the load balancer
        ec2_security_group.add_ingress_rule(security_group, ec2.Port.tcp(8443))
    
        # add ingress rule for the RDS instance to allow 3306 from the EC2 instance
        rds_security_group.add_ingress_rule(ec2_security_group, ec2.Port.tcp(3306))
        
        # add ingress rule for the RDS instance to allow 22 from the EC2 instance
        rds_security_group.add_ingress_rule(ec2_security_group, ec2.Port.tcp(22))
        
        # create an rds aurora mysql cluster
        cluster = rds.DatabaseCluster(self, "DatabaseCluster",
            engine = rds.DatabaseClusterEngine.aurora_mysql(version = rds.AuroraMysqlEngineVersion.VER_3_04_0),
            # credentials using testuser and password1234!
            credentials = rds.Credentials.from_password("testuser", SecretValue.unsafe_plain_text("password1234!")),
            # add default database name Population
            default_database_name = "Population",
            instance_props = {
                # add a vpc to the rds instance
                "vpc": vpc,
                # add a security group to the rds instance
                "security_groups": [rds_security_group],
                # add a private subnet to the rds instance
                "vpc_subnets": ec2.SubnetSelection(subnet_type = ec2.SubnetType.PRIVATE_ISOLATED)
            },
            instances = 1
        )
            
        # create an Amazon linux 2 image
        amazon_linux_2 = ec2.MachineImage.latest_amazon_linux(
            generation = ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
            edition = ec2.AmazonLinuxEdition.STANDARD,
            virtualization = ec2.AmazonLinuxVirt.HVM,
            storage = ec2.AmazonLinuxStorage.GENERAL_PURPOSE
            )
            
        # read userdata file from cdkapp directory
        with open("./cdkapp/userdata.sh") as f:
            user_data = f.read()
        
        # create a t2.small ec2 instance for the web server in a private egress subnet and vpc.availability_zones[0]
        web_server = ec2.Instance(self, "WebServer",
            instance_type = ec2.InstanceType("t2.small"),
            machine_image = amazon_linux_2,
            vpc = vpc,
            vpc_subnets = ec2.SubnetSelection(subnet_type = ec2.SubnetType.PRIVATE_WITH_EGRESS),
            security_group = ec2_security_group,
            user_data = ec2.UserData.custom(user_data),
            availability_zone = vpc.availability_zones[0],
            # add an existing role with name ec2_instance_role
            role = iam.Role.from_role_name(self, "ec2_instance_role", "ec2_instance_role")
        )
        
        # create a load balancer for the web server
        load_balancer = elbv2.ApplicationLoadBalancer(self, "LoadBalancer",
            vpc = vpc,
            internet_facing = True,
            security_group = security_group
        )
        # create a listener for the load balancer
        listener = load_balancer.add_listener("Listener",
            port = 80,
            default_action = elbv2.ListenerAction.fixed_response(status_code = 404),
            open = True
        )
        # add targets to the load balancer
        listener.add_targets("Target",
            port = 80)
            
        # add depends on for the web server to wait for the RDS cluster
        web_server.node.add_dependency(cluster)
        
        # add depends on for the listener to wait for the web server
        listener.node.add_dependency(web_server)