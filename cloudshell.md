# Cloudshell

    AWS CloudShell est un shell basé sur un navigateur qui facilite la gestion, l'exploration et l'interaction sécurisée
    avec vos ressources AWS. Avec CloudShell, vous pouvez exécuter rapidement des scripts avec l'interface de 
    ligne de commande AWS (AWS CLI), tester les API de service AWS à l'aide des kits SDK AWS ou utiliser 
    une gamme d'autres outils.

    CloudShell hérite des informations d'identification de l'utilisateur connecté à AWS Management Console.

    Un environnement Amazon Linux 2 entièrement géré qui dispose des dernières versions des outils populaires déjà 
    installés et mis à jour.

    Avec 1 Go de stockage persistant par région, vous pouvez stocker des scripts, des fichiers, des préférences 
    de configuration, etc. dans votre répertoire personnel.


