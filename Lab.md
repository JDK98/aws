LABS AWS EFFECTUES :
---


- [ ] ~~Introduction to Amazon Elastic Container Service (ECS)~~
- [ ] ~~Balance load across containers through Application Load Balancer in ECS~~
- [ ] ~~Configure Application Auto Scaling using Target Tracking policy in ECS~~
- [ ] ~~Use Amazon CloudFront for dynamic Websites~~ 
- [ ] ~~Using AWS S3 to Store ELB Access Logs~~
- [ ] ~~Creating a User Pool in AWS Cognito~~
- [ ] ~~AWS Lambda Versioning and alias from the CLI~~
- [ ] ~~Build API Gateway with different stages using stage variables~~
- [ ] ~~Configuring DynamoDB Streams Using Lambda~~
- [ ] ~~Creating and configuring a network load balancer in AWS~~
- [ ] ~~Introduction to S3~~
- [ ] ~~How to enable Versionning Amazon S3~~
- [ ] ~~Creating an S3 Lifecycle Policy~~
- [ ] ~~Introduction to Amazon Cloudfront~~
- [ ] ~~Understanding Cloudfront Origin Group~~
- [ ] ~~Introduction to Amazon EC2~~
- [ ] ~~Launch spot instance with Amazon EC2~~
- [ ] ~~Create Elastic Network Interface - Multiple IP on an EC2~~
- [ ] ~~Allocating Elastic IP and Associating it to EC2 Instance~~
- [ ] ~~Creating and Subscribing to SNS Topics, Adding SNS event for S3 bucket~~
- [ ] ~~Host Static Website Using Amazon S3~~
- [ ] ~~Accessing S3 with IAM Roles~~
- [ ] ~~AWS S3 Multipart Upload using AWS CLI~~
- [ ] ~~Using AWS S3 to Store ELB Access Logs~~
- [ ] ~~Generate S3 presign URL using CloudShell~~
- [ ] ~~Introduction to AWS Relational Database Service~~
- [ ] ~~Introduction to AWS Elastic Load Balancing~~
- [ ] ~~Creating an application load balancer from AWS CLI~~
- [ ] ~~Introduction to Amazon Auto Scaling~~
- [ ] ~~Create and Configure Amazon EC2 Auto Scaling with Launch Templates~~
- [ ] ~~Using CloudWatch for Resource Monitoring, Create CloudWatch Alarms and Dashboards~~
- [ ] ~~Introduction to AWS Elastic Beanstalk~~
- [ ] ~~Adding a Database to Elastic Beanstalk Environment~~
- [ ] ~~Blue/Green Deployments with Elastic Beanstalk~~
- [ ] ~~Introduction to AWS DynamoDB~~
- [ ] ~~DynamoDB & Global Secondary Index~~
- [ ] ~~Import CSV Data into DynamoDB~~
- [ ] ~~Access DynamoDB DAX Cluster by installing sample application on EC2 Instance~~
- [ ] ~~Import JSON Data into DynamoDB~~
- [ ] ~~Creating Events in CloudWatch~~
- [ ] ~~Create a CloudWatch event to periodically take backup of MySQL RDS table to DynamoDB~~
- [ ] ~~Launch Amazon EC2 instance, Launch Amazon RDS Instance, Connecting RDS from EC2 Instance~~
- [ ] ~~Introduction to Amazon Lambda~~
- [ ] ~~Launch an EC2 Instance with Lambda ~~
- [ ] ~~Install Python modules in AWS Lambda using Cloud9 ~~
- [ ] ~~AWS EC2 Provisioning - Cloudformation ~~
- [ ] ~~Introduction to Amazon CloudFormation ~~
- [ ] ~~How to Create Virtual Private Cloud (VPC) with AWS CloudFormation ~~
- [ ] ~~Update CloudFormation stack by creating Change set ~~
- [ ] ~~Create a VPC using AWS CLI commands  ~~
- [ ] ~~AWS Cloudformation Nested Stacks  ~~
- [ ] ~~Deploying Lambda Functions using CloudFormation  ~~













 
















































































