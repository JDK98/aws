Elastic Container Service
---

ECS est un service AWS qui permet la gestion de conteneur <br>
IL est désigné pour les applications peu complexe et est AWS proprietary <br>
C'est à dire que la migration vers un autre d'un cluster crée avec ECS sera compliqué <br>
Il propose deux méthodes: EC2 et Fargate  <br>

## EC2
Dans le Modèle EC2, les conteneurs managés sont déployé sur des instances EC2  <br>
Il va avec, un management utilisateur de l'infrastructure qui sera créee <br>

## Fargate
Fargate est un service serverless de gestion des conteneurs. <br>
Dans ce schéma, le management de l'infrastructure est AWS managed <br>
Les conteneurs selon leurs besoins sont déployés sur des instances managé par AWS <br>
Il s'agit donc de Container + infrastructure Managé  <br>
