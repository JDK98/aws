Instructions
---

# Qu'est ce qu'un ALB ?

👉‍ ALB distribue le trafic soit à des Instances EC2, des Conteneurs ou des adresses IP <br>

👉‍ Cross Zone balancing est toujours activé avec ALB <br>

👉‍ Application Load Balancer est utilisé pour les applications nécessitant des fonctionnalités avancées et une prise en charge au niveau de l'application. <br>

👉‍ Cela fonctionne au niveau de la couche application qui est la couche 7 dans le modèle OSI. <br>

👉‍ Il prend uniquement en charge les protocoles HTTP et HTTPS. <br>

👉‍ ALB a des groupes cibles (Target Groups) qui auront des cibles (Target) enregistrées telles que des instances EC2. <br>

👉‍ ALB achemine le trafic vers la cible spécifique en fonction de règles, même si le contenu des instances cibles est différent. <br>

👉‍ L'équilibreur de charge d'application agit comme un point de contact unique, qui gère le trafic entrant. <br>

👉‍ Les demandes de connexion aux instances sont gérées par le load balancer à l'aide de listeners. <br>

👉‍ Les Listeners sont configurés avec des numéros de protocole et de port et les écouteurs sont également configurés avec des règles pour acheminer le trafic vers les cibles enregistrées. <br>

👉‍ Le listener doit avoir une règle par défaut pour que les requêtes entrantes y soient acheminées par défaut. D'autres règles peuvent être configurées avec des actions adaptées aux conditions et à la priorité. <br>

👉‍ Lorsque la demande entrante correspond à la condition définie dans la règle d'écoute, ALB achemine la demande vers ce groupe cible particulier. <br>

👉‍ Le groupe cible achemine la demande vers l'instance EC2 enregistrée à l'aide du protocole et du numéro de port. <br>

👉‍ Une cible peut être enregistrée auprès de plusieurs groupes cibles et les configurations de vérification de l'état peuvent être effectuées séparément. <br>

👉‍ Une fois que l'équilibreur de charge reçoit la demande, il vérifie les règles de l'écouteur en fonction de son ordre de priorité et décide quelle règle appliquer. <br>

👉‍ Les règles d'écoute peuvent également être configurées pour acheminer le trafic vers les groupes cibles en fonction du contenu du trafic de l'application. <br>

# ETAPE DE MISE EN PLACE

1. Créer EC2 Instance <br>

2. Créer seconde instance EC2 <br>

3. Créer un ALB avec AWS CLI <br>

4. Créer deux target groups <br>

5. Enregistrer les targets avec leur targets groups repsectifs <br>

6. Créer les règles par défaut du listener <br>

7. Créer listener pour les autres règles <br>

8. Check la santé de chaque target groups <br>

9. Test de l'ALB <br>

10. Suppression des ressources <br>


# MISE EN PLACE

**1. Créer EC2 Instance** <br>

Créer une Instance EC2 avec en user data <br>

```sh
#!/bin/bash

sudo su

yum update -y

yum install httpd -y

touch /var/www/html/index.html

echo “REQUEST SERVED FROM INSTANCE1” >> /var/www/html/index.html

chmod 777 /var/www/html/index.html

mkdir -p /var/www/html/work

touch /var/www/html/work/test.html

echo “REQUEST SERVED FROM WORK PATH OF INSTANCE1” >> /var/www/html/work/test.html

mkdir -p /var/www/html/images

touch /var/www/html/images/test.html

echo “REQUEST SERVED FROM IMAGES PATH OF INSTANCE1” >> /var/www/html/images/test.html

systemctl start httpd

systemctl enable httpd
```

**2. Créer seconde instance EC2** <br>

Créer une seconde instance dans une zone de disponibilité différente de la première instance <br>

```sh
#!/bin/bash

sudo su

yum update -y

yum install httpd -y

touch /var/www/html/index.html

echo “REQUEST SERVED FROM INSTANCE2” >> /var/www/html/index.html

chmod 777 /var/www/html/index.html

mkdir -p /var/www/html/work

touch /var/www/html/work/test.html

echo “REQUEST SERVED FROM WORK PATH OF INSTANCE2” >> /var/www/html/work/test.html

chmod 777 /var/www/html/work/test.html

mkdir -p /var/www/html/images

touch /var/www/html/images/test.html

echo “REQUEST SERVED FROM IMAGES PATH OF INSTANCE2” >> /var/www/html/images/test.html

chmod 777 /var/www/html/images/test.html

systemctl start httpd

systemctl enable httpd

```

**3. Créer un ALB avec AWS CLI** <br>

📝 Copier le subnet ID de Instance 1 <br>
📝 Copier le subnet ID de Instance 2 <br>
📝 Copier le ID du Security Group à lier à l'ALB <br>

```sh
aws elbv2 create-load-balancer --name <ALB name> --subnets <Subnet Id of Instance 1> <Subnet Id of Instance 2> --security-groups <Security Group ID>
```

**4. Créer deux target groups** <br>

📝 Copier le VPC-ID du load banacer <br>
📝 Copier l'ARN du load balancer <br>

Changer les noms des target groups à chaque éxécution <br>
```sh
aws elbv2 create-target-group --name <target group name> --protocol HTTP --port 80 --vpc-id <Load balancer VPC Id>
```

**5. Enregistrer les targets avec leur targets groups repsectifs** <br>

📝 Copier l'instance ID de l'instance 1 <br>
📝 Copier l'instance ID de l'instance 2 <br>
📝 Copier l'ARN du Target group 1 <br>
📝 Copier l'ARN du Target group 2 <br>

Enregistrer les instances avec leur target group respectif <br>

```sh
aws elbv2 register-targets --target-group-arn <TG ARN> --targets Id=<Instance Id>
```

**6. Créer les règles par défaut du listener** <br>

```sh
aws elbv2 create-listener --load-balancer-arn <Load Balancer ARN> --protocol HTTP --port 80 --default-actions Type=forward,TargetGroupArn=<Target group 1 ARN>
```

**7. Créer listener pour les autres règles** <br>

📝 Copier l'ARN du listener <br>

```sh
aws elbv2 create-rule --listener-arn <LB Listeners ARN> --priority 10 --conditions Field=path-pattern,Values='/images/*' --actions Type=forward,TargetGroupArn=<TG1 ARN>
```

```sh
aws elbv2 create-rule --listener-arn <LB Listeners ARN> --priority 5 --conditions Field=path-pattern,Values='/work/*' --actions Type=forward,TargetGroupArn=<TG2 ARN>
```

**8. Check la santé de chaque target groups** <br>

```sh
aws elbv2 describe-target-health --target-group-arn <TG ARN>
```

**9. Test de l'ALB** <br>

Copie du DNS Name de L'ALB en barre d'adresse <br>

**10. Suppression des ressources** <br>

**Suppression ALB** <br>
```sh
aws elbv2 delete-load-balancer --load-balancer-arn <Load Balancer ARN>
```

**Suppression des Target groups** <br>
```sh
aws elbv2 delete-target-group --target-group-arn <TG ARN>
```

**Suppression des instances EC2** <br>
```sh
aws ec2 terminate-instances --instance-ids <instance id of Instance1> <instance id of Instance2>
```




