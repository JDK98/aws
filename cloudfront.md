# CloudFront
---


    Amazon CloudFront est un réseau de diffusion de contenu (CDN) proposé par AWS.

    Le CDN fournit un réseau de serveurs proxy distribués dans le monde entier qui met en cache le contenu, c'est-à-dire les vidéos Web ou d'autres médias volumineux, plus localement pour les consommateurs, améliorant ainsi la vitesse d'accès pour le téléchargement du contenu..

    Le service CloudFront fonctionne sur une base de paiement à l'utilisation.

    CloudFront fonctionne avec des serveurs d'origine tels que S3, EC2 où le contenu est stocké et est transmis à plusieurs serveurs CloudFront lorsque le contenu est demandé.

    Lorsque CloudFront est activé, le contenu est stocké sur le serveur S3 principal.

    Des copies de ce contenu sont créées sur un réseau de serveurs à travers le monde appelé CDN.

    Chaque serveur de ce réseau est appelé un serveur Edge, qui n'aura qu'une copie de votre contenu.

    Lorsqu'une demande est faite au contenu, l'utilisateur est fourni à partir du serveur périphérique le plus proche.

    CloudFront possède des fonctionnalités similaires à l'accélération de site dynamique, une méthode utilisée pour améliorer la diffusion de contenu en ligne.

    CloudFront accélère la diffusion de contenu dynamique en le rapprochant de l'utilisateur afin de minimiser les sauts Internet impliqués dans la récupération du contenu.

    La distribution Web de CloudFront prend en charge le téléchargement "progressif", c'est-à-dire que les données de S3 sont mises en cache puis diffusées en continu sans interruption.

    Pour cette raison, l'utilisateur ne peut pas se déplacer vers l'avant ou vers l'arrière dans la vidéo, c'est-à-dire que la vidéo est traitée petit à petit.

    La prise en charge de la distribution Web de CloudFront "Streaming" permet aux utilisateurs de regarder directement sans aucun téléchargement.

    Ce service est avantageux pour ceux qui développent un site Web qui distribue beaucoup de contenu et doit évoluer.

    Il aide à réduire les coûts et à améliorer les performances d'un site Web en offrant des vitesses de transfert de données élevées et une faible latence.

