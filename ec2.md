EC2
---

👉‍ EC2 pour Elastic Compute Cloud. <br><br>

👉‍ C'est un environnement virtuel où "vous louez" pour faire créer votre environnement, sans acheter. <br><br>

👉‍ Ces machines virtuelles sont appelées des Instances. <br><br>

👉‍ Des modèles préconfigurés peuvent être utilisés pour lancer des instances. Ces modèles sont appelés images. Amazon fournit ces images sous forme d'AMI (Amazon Machine Images). <br><br>

👉‍ Ces instances vous permettent d'installer des applications et des services personnalisés. <br><br>

👉‍ La mise à l'échelle de l'infrastructure, c'est-à-dire à la hausse ou à la baisse, est facile en fonction de la demande à laquelle vous faites face. <br><br>

👉‍ AWS fournit plusieurs configurations de processeur, de mémoire, de stockage, etc., grâce auxquelles vous pouvez choisir la version requise pour votre environnement. <br><br>

👉‍ Aucune limitation de stockage. Vous pouvez choisir le stockage en fonction du type d'instance sur laquelle vous travaillez. <br><br>

👉‍ Des volumes de stockage temporaires sont fournis, appelés "Instance Store". Les données stockées dans celui-ci sont supprimées une fois l'instance terminée. <br><br>

👉‍ Des volumes de stockage persistants sont disponibles et sont appelés volumes EBS (Elastic Block Store). <br><br>

👉‍ Ces instances peuvent être placées à plusieurs emplacements appelés régions et zones de disponibilité (AZ). <br><br>

👉‍ Vous pouvez répartir vos instances sur plusieurs AZ, c'est-à-dire dans une seule région, de sorte qu'en cas d'échec d'une instance, AWS remappe automatiquement l'adresse vers une autre AZ. <br><br>

👉‍ Les instances déployées dans une AZ peuvent être migrées vers une autre AZ. <br><br>

👉‍ Pour gérer les instances, les images et d'autres ressources EC2, vous pouvez éventuellement attribuer vos propres métadonnées à chaque ressource sous la forme de balises. <br><br>

👉‍ Une balise est une étiquette que vous attribuez à une ressource AWS. Il contient une clé et une valeur facultative, toutes deux définies par vous. <br><br>

👉‍ Chaque compte AWS est livré avec un ensemble de limites par défaut sur les ressources par région. <br><br>

👉‍ Pour toute augmentation de la limite, vous devez contacter AWS. <br><br>

👉‍ Pour travailler avec les instances créées, nous utilisons des paires de clés. <br><br>


# Elastic Network Interface



    Network Interface is a network card for the virtual machine, multiple ENI's can be attached to a single EC2 instance.

    By default, it gives you a private IPv4 address, you can choose to attach an Elastic IP for a Public IPv4 address.

    While attaching ENI's to EC2 instances, make sure to have your EC2 Instance in the same subnet as ENI.

    You will charged for an Elastic IP Address that is associated with a network interface but the network interface isn’t attached to a running instance.

    You can attach and detach secondary interfaces (eth1-ethn) on an EC2 instance, but you can’t detach the eth0 interface.





