# DYNAMO DB

👉‍  Amazon DynamoDB est un service de base de données NoSQL entièrement géré où la maintenance, la charge administrative, l'exploitation et la mise à l'échelle sont prises en charge par AWS. <br> <br>

👉‍   Nous n'avons pas besoin de fournir les spécifications du montant que nous allons économiser. <br>

👉‍   Il fournit une latence à un chiffre même pour des téraoctets de données et est utilisé pour les applications nécessitant des lectures très rapides. <br> <br>

👉‍   Il est utilisé dans des applications comme les jeux où les données doivent être capturées et les changements se produisent très rapidement. <br> <br>

    1 RCU & 1 WCU

    Kb/second

          |   Strong consistency  : 4KB
     Read |    
          |    Eventual Consistency : 8KB

           |
    Write  |   1KB
           |

<br>

## Amazon DynamoDB Streams

👉‍  Le flux Amazon DynamoDB est une fonctionnalité qui émet des événements lorsque des modifications ou des changements d'enregistrement se produisent dans une table DynamoDB.  <br> <br>

👉‍  Les flux DynamoDB capturent la modification du niveau de temps dans la table DynamoDB dans une séquence chronologique.  <br> <br>

👉‍  Lorsqu'un flux DynamoDB est activé, il capture les modifications apportées aux tables DynamoDB de manière ordonnée. <br> <br>

👉‍  Les flux DynamoDB peuvent être utilisés pour répliquer les données d'une table DynamoDB de la région 1 vers une autre table DynamoDB de la région 2. <br> <br>

👉‍  Les événements peuvent être des types suivants: INSERT, UPDATE, REMOVE <br> <br>

👉‍  Il enregistre toutes les modifications via des journaux cryptés et les stocke pendant 24 heures. <br> <br>

👉‍  Chaque fois qu'il y a un changement, DynamoDB crée un enregistrement de flux avec l'attribut de clé primaire des éléments. <br> <br>

👉‍ Les enregistrements de flux sont couplés en groupes appelés shard. Chaque shard contient plusieurs enregistrements de flux. <br> <br>

👉‍ Le shard contient également les informations requises pour accéder aux enregistrements de flux. <br> <br>

👉‍ Les événements sont enregistrés en temps quasi réel.    <br> <br>

👉‍ Dans les applications en temps réel, nous pouvons accéder aux enregistrements de flux qui contiennent des événements où des changements ont lieu. <br><br>

👉‍ Les flux peuvent être activés lors de la création d'une table DynamoDB et peuvent être désactivés à tout moment. <br> <br>

👉‍ Une fois le flux DynamoDB désactivé, les données du flux seront disponibles pendant 24 heures. Il n'y a pas de méthodologie disponible pour supprimer manuellement les flux existants. <br><br>

👉‍ Les flux DynamoDB peuvent être utilisés comme source d'événements pour les fonctions Lamda afin que vous puissiez créer des applications qui prennent des mesures en fonction des événements de la table DynamoDB. <br> <br>

👉‍ Les événements capturés par DynamoDB peuvent être analysés en déplaçant les données vers d'autres services AWS comme S3 ou Cloudwatch. <br> <br>


