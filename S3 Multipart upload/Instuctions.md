INSTRUCTIONS
---

# Qu'est ce que le Multipart Upload ?

👉 Permet de transférer de gros objets en les scindant en différentes parties <br>

👉 Ses différentes parties peuvent être transmises en ordre aléatoire  <br>

👉 Si la transmission d'une partie échoue, on peut renvoyer uniquement cette partie et non tout l'objet <br>

👉 Après la transmission de toutes les parties, Amazon S3 sait assembler ses parties et créer l'objet <br>

👉 En best practice, dès que la taille de l'objet à transférer est supérieur à 100Mb, utiliser Multipart Upload plutôt qu'envoie par unique opération  <br>

# Etape de Mise en place

**1. Créer un IAM Role** <br>
**2. Créer un S3 bucket** <br>
**3. Créer une instance EC2** <br>
**4. SSH l'instance EC2** <br>
**5. Découpe du fichier original** <br>
**6. Créer un Multipart upload** <br>
**7. Upload des parties du fichier** <br>
**8. Créer un fichier JSON Multipart** <br>
**9. Terminer le Multipart Upload** <br>

# Mise en place
**1. Créer un IAM Role** <br>

Créer un EC2 IAM Role avec Full Access sur S3 <br>

**2. Créer un S3 Bucket**

Créer un S3 Bucket avec les parametrages par défaut

**3. Créer une Instance EC2**

Créer une instance EC2 utilisant le IAM Role crée précédemment et avec le port SSH ouvert en SG

**4. SSH l'instance EC2**

Accéder au serveur avec la clé fourni à la création de l'instance

**5. Découpe du fichier original**

- Telecharger le fichier original depuis le bucket source <br>
`aws s3 cp s3://source-bucket-multipart/nodeJS.mp4 /home/ubuntu`

- Découpe du fichier en partie de 40Mb <br>
`split -b 40M nodeJS.mp4`

**6. Créer un Multipart Upload** <br>
`aws s3api create-multipart-upload --bucket dest-bucket-multipart --key nodeJS.mp4`

📝 **Note: Copier la sortie "UploadId"**

**7. Upload des parties du fichier**
| body | Part Number |
| ------ | ------ |
| xaa | 1 |
| xab | 2 |
| xac | 3 | 

```
aws s3api upload-part --bucket dest-bucket-multipart \
--key nodeJS.mp4 \ 
--part-number 1 \
--body xaa \
--upload-id [id]
```

📝 **NOTE : REPETER LA COMMANDE POUR CHAQUE PARTIE ET NOTER LES ETAG**

**8. Créer un fichier JSON Multipart**

Placez tous les Etag enregistrés dans un fichier <br>
Le fichier ressemblera à ceci <br>

**_list.json_**

```json
{

  "Parts": [

    {

      "PartNumber": 1,

      "ETag": "\"e604e1f72752b2f51ff57c533f685a24\""

    },

    {

      "PartNumber": 2,

      "ETag": "\"3f8aba6b9c4d7b6a00667063eef23de6\""

    },

    {

      "PartNumber": 3,

      "ETag": "\"2a7081477570135c258eb0c2543a277a\""

    }

  ]

}
```

**9. Terminer le Multipart upload**

```
aws s3api complete-multipart-upload \
--multipart-upload file://list.json \
--bucket dest-bucket-multipart \
--key nodeJS.mp4 \
--upload-id [upload id]
```
