SNS
---

👉 SNS : Simple Notification Service <br>

👉 Fournit une infrastructure à faible coût pour la diffusion massive de messages, principalement aux utilisateurs mobiles. <br>

👉 SNS agit comme un bus de messages unique qui peut envoyer des messages à une variété d'appareils et de plates-formes. <br>

👉 SNS utilise le modèle de "publish/subscribe" pour la livraison push des messages <br>

👉 SNS permet de dissocier les microservices, les systèmes distribués et les applications serverless à l'aide de pub/sub entièrement géré. <br>

👉 Les "Publishers" communiquent de manière asynchrone avec les "Subscribers" en produisant et en envoyant un message à un "Topic", qui est un point d'accès logique et un canal de communication. <br>

👉 Les abonnés, c'est-à-dire les serveurs Web, les adresses e-mail, les files d'attente SQS, etc., consomment ou reçoivent le message ou la notification via l'un des protocoles pris en charge lorsqu'ils sont abonnés au "Topic". <br>

👉 Les destinataires s'abonnent à un ou plusieurs "Topics" dans SNS. <br>

👉 À l'aide des topics SNS, les "publishers" peuvent diffuser des messages vers un grand nombre de points de terminaison de "Subscribers" pour un traitement parallèle, y compris les files d'attente Amazon SQS, les fonctions AWS Lambda et les webhooks HTTP/S. <br>

👉 SNS peut aider à dimensionner automatiquement la charge de travail. <br>
