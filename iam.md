IAM
---

**Identity and Access Management** <br>
Il s'agit du service AWS de gestion des utilisateurs, groupes, permissions etc

`arn:partition:service:region:account:resource` <br>
Sample: `arn:aws:iam::123456789012:user/bob`

**POLICE** <br>
--- 
Les polices sont des (permissions | interdictions) pouvant être attribuées à des users, des groupes, des roles ou des ressources <br>
Ils assignent des "Long term security credentials" (Access Key ID, Secret Access Key)
Il existe deux types de polices : 
- **IAM based Identity policies** <br>
    Il s'agit ici des permissions attribuées aux users, groupes ou role <br>
    On en distingue deux:  <br>
    | Managed Policies | Inline Policies |
    | ------ | ------ |

- **IAM Resource based policies** <br>
    Il s'agit ici des permissions attribuées aux ressources telles que S3 <br>
    Elles définissent quelles actions peuvent être exécutées sur les ressources et sous quelles conditions  <br>
    Ce sont des Inline Policies et il n'y a pas de Resource Based Policies managé par AWS <br>
  
**IAM POLICY JSON FORMAT** <br>
Autoriser toutes les actions dynamodb sur la ressource table/books <br>
[Identity Based Policies] <br>
```
{
    "Version":"2012-10-17",
    "Statement": {
        "Effect": "Allow",
        "Action": "dynamodb:*",
        "Resource": "arn:aws:dynamodb:us-east-2:123456789012:table/Books"
    }
}
```
Autoriser l'utilisateur bob à executer des actions "PutObject" et "PutObjectAcl" <br>
[Resource Based Policies] <br>
```
{
    "Version":"2012-10-17",
    "Statement": {
        "Effect": "Allow",
        "Principal": {"AWS":"arn:aws:iam::77778888999:user/bob"},
        "Action": [
            "s3": "PutObject",
            "s3": "PutObjectAcl"
        ]
    }
}
```

Autoriser tout le monde à lire les objets du bucket s3introductionlabs
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Statement1",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::s3introductionlabs/*"
        }
    ]
}
```

Autoriser les actions Lire et Ecrire Objets sur les buckets spécifiés
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action":[
                    "s3:GetObject"
            ],
            "Resource":[
                    "arn:aws:s3:::bucket-source-lab/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                    "s3:PutObject"
                ],
            "Resource": [
                    "arn:aws:s3:::dest-bucket-lab/*"
                ]
        }

    ]
}
```

Permet des actions sur EC2 dans la region us-east-1
```

{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:Describe*",
                "ec2:CreateKeyPair",
                "ec2:CreateSecurityGroup",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:AuthorizeSecurityGroupEgress",
                "ec2:CreateTags",
                "ec2:DescribeTags",
                "ec2:RunInstances"
            ],
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "ec2:Region": "us-east-1"
                }
            }
        }
    ]
}

```

**NB <br>
Default policy is Deny <br>
Explicit Allow override the Default policy <br>
Explicit Deny override the Allow <br>
If none of them then Default Policy** <br>

**ROLE** <br>
---
Les IAM Role génèrent dynamiquement des "temporary security credentials" ce qui est une manière plus sécurisé de fournir des accès  <br>
Les roles sont utilisés pour fournir des accès aux utilisateurs, applications ou services qui n'ont pas accès aux ressources AWS  <br>
Un role est un ensemble de permissions qui assignent des droits aux ressources AWS, ces permissions sont assignés aux rôles  <br>
pas aux utilisateurs ni aux groupes  <br>

