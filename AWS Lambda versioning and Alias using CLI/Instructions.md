Instructions
---

1. **Creér IAM Role pour Lambda** <br>

Créer un IAM role Lambda avec les permissions suivantes (Full access sur S3)

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": "*"
        }
    ]
}
```

📝 Noter l'ARN de l'IAM <br>


2. Attribuer des permissions à votre instance EC2 <br>

Dans l'option où vous utilisez une instance EC2 pour l'administration, ajoutez lui les permissions suivantes <br>

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1575022704273",
            "Effect": "Allow",
            "Action": [
                "iam:PassRole",
                "iam:CreateRole",
                "lambda:CreateAlias",
                "lambda:CreateFunction",
                "lambda:GetFunctionConfiguration",
                "lambda:DeleteAlias",
                "lambda:DeleteFunction",
                "lambda:Get*",
                "lambda:InvokeFunction",
                "lambda:List*",
                "lambda:PublishVersion",
                "lambda:UpdateAlias",
                "lambda:UpdateVersion",
                "lambda:UpdateFunctionCode",
                "lambda:UpdateFunctionConfiguration"
            ],
            "Resource": "*"
        }
    ]
}
```

3. Créer une fonction Lambda avec AWS CLI  <br>

-  Créer le code Lambda  <br>
_**s3bucket.py**_
```py
import logging
import boto3
import json
from botocore.exceptions import ClientError
def handler(event, context):
    s3 = boto3.resource('s3', region_name='us-east-1')
    s3.create_bucket(Bucket='<bucketName>')
    content="File uploaded by version 1"
    responses3 = s3.Object('<bucketName>', 'version1.txt').put(Body=content)
    print("uploading file completed")
```

```sh
zip s3bucket.zip s3bucket.py
```

-  Créer la fonction lambda <br>

```sh
aws lambda create-function --function-name lambdaclidemo --runtime python3.7 --zip-file fileb://s3bucket.zip --handler s3bucket.handler --role <arn_iam_role>
```
📝 La version du code sera $LATEST <br>

**[Optional]. Changer le timeout period de la fonction <br>**

```sh
aws lambda update-function-configuration  --function-name lambdaclidemo --timeout 15
```

**[Optional]. Invoquer la fonction lambda en CLI <br>**

```sh
aws lambda invoke --function-name lambdaclidemo --invocation-type RequestResponse  outputfile.txt
```

4. Publier une nouvelle version de la fonction <br>

```sh
aws lambda publish-version --function-name lambdaclidemo
```
📝 Dans l'onglet Version de la fonction Lambda, vous pouvez voir indiquer "version 1" <br>

    Modifions le code de la fonction Lambda et redéployons la

_**s3bucket.py**_
```py
import logging
import boto3
import json
from botocore.exceptions import ClientError
def handler(event, context):
    s3 = boto3.resource('s3', region_name='us-east-1')
    s3.create_bucket(Bucket='<bucketName>')
    content="File uploaded by version 2"
    responses3 = s3.Object('<bucketName>', 'version2.txt').put(Body=content)
    print("uploading file completed")
```

```sh
zip s3bucket.zip s3bucket.py
```

**Upload du nouveau zip à la fonction Lambda** <br>

```sh
aws lambda update-function-code --function-name lambdaclidemo --zip-file fileb://s3bucket.zip
```

**Invoquer la fonction Lambda** <br>
```sh
aws lambda invoke --function-name lambdaclidemo --invocation-type RequestResponse  outputfile.txt
```

5. Créer et supprimer un alias <br>

```sh
aws lambda create-alias --function-name lambdaclidemo --description "sample alias for lambda" --function-version 1 --name DEV
```

```sh
aws lambda delete-alias --function-name lambdaclidemo --name DEV
```

6. Supprimer la fonction Lambda

```sh
aws lambda delete-function --function-name lambdaclidemo
```







 






