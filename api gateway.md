API GATEWAY
---

👉‍ Amazon API Gateway est un service entièrement géré qui permet aux développeurs de créer, publier, maintenir, surveiller et sécuriser facilement des API à n'importe quelle échelle. 

👉‍ Les API agissent comme la porte d'entrée permettant aux applications d'accéder aux données, à la logique métier ou aux fonctionnalités de vos services backend. 

👉‍ API Gateway gère toutes les tâches impliquées dans l'acceptation et le traitement de centaines de milliers d'appels d'API simultanés, y compris la gestion du trafic, la prise en charge de CORS, l'autorisation et le contrôle d'accès, la limitation, la surveillance et la gestion des versions d'API. 

👉‍ À l'aide d'API Gateway, vous pouvez créer des API RESTful et des API WebSocket qui permettent des applications de communication bidirectionnelle en temps réel. API Gateway prend en charge les charges de travail conteneurisées et sans serveur, ainsi que les applications Web. 

