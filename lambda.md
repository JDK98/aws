AWS LAMBDA 
---

👉‍ Le service AWS Lambda vous permet d'exécuter du code sans provisionner ni gérer de serveurs. <br>

👉‍ La caractéristique intéressante de lambda est que vous n'avez qu'à payer pour le temps de calcul que vous consommez et que vous n'avez pas besoin de payer lorsque votre code ne s'exécute pas. <br>

👉‍ vous pouvez exécuter du code pour pratiquement n'importe quel type d'application sans aucune administration à l'aide des fonctions AWS Lambda. <br>

👉‍ Téléchargez simplement votre code sur Lambda et il se chargera de tout ce qui est nécessaire pour exécuter et mettre à l'échelle votre code avec une haute disponibilité <br>

👉‍ Nous pouvons définir des événements déclencheurs pour notre fonction lambda quand s'exécuter ou quand se déclencher. <br>

👉‍ Lambda prend actuellement en charge divers langages tels que java, python, node js, c, etc. à l'aide desquels vous pouvez écrire votre fonction lambda. <br>

# Lambda Version and Alias

👉‍ Il est possible d'utiliser des versions lambda pour gérer le déploiement de vos fonctions AWS Lambda <br>

👉‍ La fonction Lambda crée une nouvelle version chaque fois que vous publiez la fonction. <br>

👉‍ La nouvelle version créée est une copie de la version non publiée de la fonction. <br>

👉‍ Lambda nous permet de modifier le code et les paramètres de la fonction uniquement sur la version non publiée d'une fonction. <br>

👉‍ Chaque version de votre fonction Lambda a son propre ARN. <br>

👉‍ Une fois que la fonction a publié le code et la plupart des paramètres sont verrouillés pour assurer une expérience cohérente pour les utilisateurs de cette version et vous ne pouvez pas éditer ou modifier le code de cette version. <br>

👉‍ Un alias Lambda agit comme un pointeur vers une version de fonction Lambda spécifique. <br>

👉‍ AWS nous permet de créer un ou plusieurs alias pour la fonction lambda particulière. <br>

👉‍ Chaque alias a son propre ARN unique comme des versions et pointe vers une version spécifique et ne peut pas pointer un alias vers d'autres. <br>

👉‍ Vous pouvez mettre à jour un alias pour pointer vers une nouvelle version de la fonction qui pointe vers une autre fonction. <br>
