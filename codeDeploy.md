AWS CODE DEPLOY
---

👉‍  AWS CodeDeploy vous aide à automatiser le déploiement de votre application d'une variété de services de calcul tels qu'Amazon EC2, AWS Lambda, AWS Fargate et les serveurs on premise. <br> <br>

👉‍  En utilisant CodeDeploy, nous pouvons déployer les types de contenu suivants:

        Code

        Fonction AWS Lambda

        Fichier Web et de Configurations 

        Paquets d'exécutable

        Scripts

        Fichiers Multimedia

👉‍  Le code source de l'application peut être stocké dans S3 Bucket, CodeCommit, des référentiels basés sur git tels que GitHub ou BitBucket. <br> <br>

👉‍  Codedeploy vous aide à publier rapidement de nouvelles fonctionnalités, à mettre à jour la version des fonctions AWS Lambda, à éviter les temps d'arrêt lors des déploiements d'applications et à gérer la complexité des applications ayant des déploiements manuels sujets aux erreurs.. <br> <br>

👉‍  Pour travailler avec divers systèmes de gestion de configuration, de contrôle de code source, d'intégration continue (CI) et de livraison continue, vous pouvez utiliser CodeDeploy. <br> <br>
 
👉‍  CodeDeploy peut être utilisé pour les types de déploiement suivants:

        Une application qui nécessite des serveurs comme Amazon EC2, On premise

        Applications serverless utilisant Lambda

        Appication conteneurisé déployé avec ECS
<br>
👉‍  AWS CodeDeploy prend en charge deux types de déploiement:

    Déploiement sur place : pour cette méthode, vous arrêtez le déploiement des applications présentes sur chaque instance et installez une application révisée. Une fois la nouvelle version de l'application déployée, l'application démarre et est validée. 

        📝 Note: Les déploiements AWS Lambda et Amazon ECS ne prennent pas en charge les déploiements sur place.

    Blue-green deployment: Pour ce type de déploiement, les caractéristiques du déploiement dépendent du type de plate-forme de calcul utilisée pour le déploiement.

            EC2 ou On-premise

            AWS Lambda

            Amazon ECS

