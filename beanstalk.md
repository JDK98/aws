Elastic Beanstalk
---

    AWS Elastic Beanstalk is a Platform as a Service(PaaS) offered by Amazon for deploying and scaling web applications

    It allows us to use a wide selection of application platforms.

    It also enables us to have a variety of application deployment options. 

    It enables the developer to concentrate on developing rather than spending time on managing and configuring servers.

    It gives us the ability to automatically scale an application up and down based on the application’s specific needs.

# Blue/Green deployments with Elastic Beanstalk


    AWS Elastic Beanstalk performs normal deployments in the current production environment. Due to this, the application will be unavailable to users until the update is complete.

    This downtime can be avoided by performing a blue/green deployment.

    Blue/Green deployments are used to update an application from one version to another version without any downtime.

    The blue environment is your existing production environment carrying live traffic.

    The Green environment is an identical parallel new environment running a different or updated version of the application.

    Deploying of Blue/Green environments such that you can deploy a new version to a separate environment.

    Once the deployment is done, we simply route the traffic from the Blue environment to Green environment by swapping their CNAMEs or using Route 53 to slowly shift percentages of traffic over (not included in this lab). 

    After the routing is complete, if you face any issues in the green environment, Elastic BeanStalk gives us an option to easily roll back to the blue environment.
