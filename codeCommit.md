AWS CODE COMMIT
---

👉‍  AWS CodeCommit est un service d'infrastructure de contrôle de version fourni par AWS qui est utilisé pour enregistrer vos données telles que des documents, des fichiers et du code source de manière sécurisée et privée dans le cloud, c'est-à-dire un référentiel interne. <br>

👉‍  Il s'agit d'un service géré avec des fonctionnalités standard de GIT qui fonctionnent également de manière transparente avec les référentiels GIT existants. <br>

👉‍  Il peut stocker n'importe quel type de code avec n'importe quel type d'extensions en toute sécurité avec moins de restrictions. <br>

👉‍  Les données stockées dans AWS CodeCommit sont cryptées afin qu'elles soient hautement sécurisées et moins vulnérables aux attaques externes. <br>

👉‍  Il permet le contrôle d'accès afin de travailler dans un environnement collaboratif. <br>

👉‍  Il peut être facilement mis à l'échelle. <br>

👉‍  Il nous permet d'intégrer les données avec d'autres outils tiers. <br>

