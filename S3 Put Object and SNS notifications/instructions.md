INSTRUCTIONS
---

# Qu'est ce que SNS ?

👉 SNS : Simple Notification Service <br>

👉 Fournit une infrastructure à faible coût pour la diffusion massive de messages, principalement aux utilisateurs mobiles. <br>

👉 SNS agit comme un bus de messages unique qui peut envoyer des messages à une variété d'appareils et de plates-formes. <br>

👉 SNS utilise le modèle de "publish/subscribe" pour la livraison push des messages <br>

👉 SNS permet de dissocier les microservices, les systèmes distribués et les applications serverless à l'aide de pub/sub entièrement géré. <br>

👉 Les "Publishers" communiquent de manière asynchrone avec les "Subscribers" en produisant et en envoyant un message à un "Topic", qui est un point d'accès logique et un canal de communication. <br>

👉 Les abonnés, c'est-à-dire les serveurs Web, les adresses e-mail, les files d'attente SQS, etc., consomment ou reçoivent le message ou la notification via l'un des protocoles pris en charge lorsqu'ils sont abonnés au "Topic". <br>

👉 Les destinataires s'abonnent à un ou plusieurs "Topics" dans SNS. <br>

👉 À l'aide des topics SNS, les "publishers" peuvent diffuser des messages vers un grand nombre de points de terminaison de "Subscribers" pour un traitement parallèle, y compris les files d'attente Amazon SQS, les fonctions AWS Lambda et les webhooks HTTP/S. <br>

👉 SNS peut aider à dimensionner automatiquement la charge de travail. <br>

# ETAPE DE MISE EN PLACE

**1. Créer un SNS Topic**

**2. S'abonner au Topic**

**3. Créer un bucket S3**

**4. Mise à jour de la police d'accès SNS**

**5. Créer un S3 Event**

**6. Tester les SNS Notifications**

# Mise en place

**1. Créer un SNS Topic**

Créer un Topic SNS de type standard
📝 Copier l'ARN du Topic

**2. S'abonner au Topic**

S'abonner au Topic et valider l'abonnement par mail

**3. Créer un bucket S3**

Créer un bucket S3
📝 Copier l'ARN du bucket

**4. Mise à jour de la police d'accès SNS**

```json
 {
  "Version": "2008-10-17",
  "Id": "__default_policy_ID",
  "Statement": [
    {
      "Sid": "__default_statement_ID",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish",
        "SNS:Receive"
      ],
      "Resource": "<Your_SNS_Topic_ARN>",
      "Condition": {
        "ArnLike": {
          "aws:SourceArn": "<Your_Bucket_ARN>"
        }
      }
    }
  ]
}
```

**5. Créer un S3 Event**

Créer un S3 Event avec Put Object en alerte 

Dans Destination, selectionnez le SNS topic créez précedemment 

**6. Tester les SNS Notifications**

Uploader des fichiers sur votre S3 bucket et recevez un mail






