## Regions, Availability Zone 
 
AWS Regions are separate geographic areas where Amazon Web Services hosts its data centers. Each AWS Region consists of multiple, isolated locations known as Availability Zones (AZs). An AZ is composed of one or more discrete data centers with independent power, networking, and connectivity, housed in separate facilities. This design ensures that AWS Regions are independent from one another, and without explicit customer consent, data is not replicated from one Region to another. When selecting an AWS Region to host your applications and workloads, consider factors such as :

#### Compliance

Enterprise companies often must comply with regulations that require customer data to be stored in a specific geographic territory. If applicable, choose a Region that meets your compliance requirements.

#### Latency 

If your application is sensitive to latency (the delay between a request for data and the response), choose a Region that is close to your user base. This helps prevent long wait times for your customers. Synchronous applications such as gaming, telephony, WebSockets, and Internet of Things (IoT) are significantly affected by high latency. Asynchronous workloads, such as ecommerce applications, can also suffer from user connectivity delays.

#### Pricing

Due to the local economy and the physical nature of operating data centers, prices vary from one Region to another. Internet connectivity, imported equipment costs, customs, real estate, and other factors impact a Region's pricing. Instead of charging a flat rate worldwide, AWS charges based on the financial factors specific to each Region.

#### Service Availability

Some services might not be available in some Regions. The AWS documentation provides a table that shows the services available in each Region.

## Edge locations

Edge locations are global locations where content is cached. Currently, there are over 400+ edge locations globally.

Amazon CloudFront delivers your content through a worldwide network of edge locations. When a user requests content that is being served with CloudFront, the request is routed to the location that provides the lowest latency. So that content is delivered with the best possible performance. CloudFront speeds up the distribution of your content by routing each user request through the AWS backbone network to the edge location that can best serve your content.


