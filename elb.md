ELASTIC LOAD BALANCER
---

👉‍ Elastic Load Balancing est l'un des services fournis par AWS pour distribuer les requêtes entrante ou le trafic réseau sur plusieurs cibles, telles que les instances EC2, les conteneurs et les adresses IP. <br>

👉‍ ELB rend vos applications hautement disponibles et tolérantes aux pannes. <br> 

👉‍ Il utilise des vérifications de l'état pour détecter quelles instances sont saines et dirige le trafic uniquement sur ces instances. <br>

👉‍ Vous pouvez ajouter et supprimer des ressources de votre équilibreur de charge en fonction de vos besoins sans perturber le flux de requêtes vers vos applications. <br>

# Les Types de Load Balancers

👉‍ **Application Load Balancer** <br>
Application Load Balancer est le mieux adapté à l'équilibrage de charge du trafic HTTP et HTTPS <br>

👉‍ **Network Load Balancer** <br>
Network Load Balancer est utilisé pour répartir le trafic ou la charge à l'aide des protocoles TCP/UDP <br>

👉‍ **Classic Load Balancer** <br>
Classic Load Balancer fournit un équilibrage de charge de base sur plusieurs instances Amazon EC2. <br>

# Application Load Balancer 

👉‍ ALB distribue le trafic soit à des Instances EC2, des Conteneurs ou des adresses IP <br>

👉‍ Cross Zone balancing est toujours activé avec ALB <br>

👉‍ Application Load Balancer est utilisé pour les applications nécessitant des fonctionnalités avancées et une prise en charge au niveau de l'application. <br>

👉‍ Cela fonctionne au niveau de la couche application qui est la couche 7 dans le modèle OSI. <br>

👉‍ Il prend uniquement en charge les protocoles HTTP et HTTPS. <br>

👉‍ ALB a des groupes cibles (Target Groups) qui auront des cibles (Target) enregistrées telles que des instances EC2. <br>

👉‍ ALB achemine le trafic vers la cible spécifique en fonction de règles, même si le contenu des instances cibles est différent. <br>

👉‍ L'équilibreur de charge d'application agit comme un point de contact unique, qui gère le trafic entrant. <br>

👉‍ Les demandes de connexion aux instances sont gérées par le load balancer à l'aide de listeners. <br>

👉‍ Les Listeners sont configurés avec des numéros de protocole et de port et les écouteurs sont également configurés avec des règles pour acheminer le trafic vers les cibles enregistrées. <br>

👉‍ Le listener doit avoir une règle par défaut pour que les requêtes entrantes y soient acheminées par défaut. D'autres règles peuvent être configurées avec des actions adaptées aux conditions et à la priorité. <br>

👉‍ Lorsque la demande entrante correspond à la condition définie dans la règle d'écoute, ALB achemine la demande vers ce groupe cible particulier. <br>

👉‍ Le groupe cible achemine la demande vers l'instance EC2 enregistrée à l'aide du protocole et du numéro de port. <br>

👉‍ Une cible peut être enregistrée auprès de plusieurs groupes cibles et les configurations de vérification de l'état peuvent être effectuées séparément. <br>

👉‍ Une fois que l'équilibreur de charge reçoit la demande, il vérifie les règles de l'écouteur en fonction de son ordre de priorité et décide quelle règle appliquer. <br>

👉‍ Les règles d'écoute peuvent également être configurées pour acheminer le trafic vers les groupes cibles en fonction du contenu du trafic de l'application. <br>


# Network Load Balancer

👉‍ L'équilibreur de charge réseau (NLB) distribue le trafic en fonction de variables réseau, telles que l'adresse IP et les ports de destination. <br>

👉‍ NLB est capable de traiter le trafic et d'évoluer à un rythme beaucoup plus élevé que l'équilibreur de charge d'application. <br>

👉‍ Nous ne pouvons pas utiliser certaines fonctionnalités de l'ALB telles que "SSL-offloading", "host-based routing", "cross-zone load balancing" etc <br>

👉‍ Il n'est pas conçu pour prendre en considération quoi que ce soit au niveau de la couche d'application, comme le type de contenu, les données de cookie, les en-têtes personnalisés, l'emplacement de l'utilisateur ou le comportement de l'application. <br>

👉‍ Pour le trafic TCP, NLB sélectionne une cible à l'aide d'un algorithme de hachage de flux basé sur le type de protocole, l'adresse IP source, le port source, l'adresse IP de destination et le port de destination. <br>

👉‍ Les connexions TCP d'un client ont des ports source et des numéros de séquence différents par rapport à NLB et peuvent être acheminées vers différentes cibles.  <br>

👉‍ Chaque connexion TCP est acheminée vers une cible unique pour une connexion. <br>

👉‍ L'avantage de NLB est qu'il peut gérer le trafic vers un port différent vers la même instance. <br>

👉‍ Nous pouvons diviser la demande en fonction du port vers différents services à l'aide du Network Load Balancer, ainsi NLB vous permet d'acheminer le trafic entre plusieurs applications exécutées sur le même serveur. <br>

