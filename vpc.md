# Virtual Private Cloud

👉‍ VPC signifie Virtual Private Cloud. <br>

👉‍ Il s'agit d'un réseau virtuel personnalisé au sein du cloud AWS. <br>

👉‍ Les utilisateurs peuvent logiquement créer leur réseau personnel, en concevant et en mettant en œuvre un réseau séparé et indépendant qui fonctionnerait dans le Cloud AWS. <br>

👉‍ Les principaux composants sont : les sous-réseaux, les adresses IP, les périphériques NAT (instances et passerelles), les tables de routage, les passerelles Internet et privées virtuelles, les listes de contrôle d'accès, les groupes de sécurité, les points de terminaison VPC. <br>

👉‍ Un sous-réseau est un segment de la plage d'adresses IP VPC, où nous pouvons lancer des instances EC2, RDS et d'autres ressources AWS. <br>

👉‍ Les sous-réseaux sont en outre classés comme public et privé. <br>

👉‍ Les sous-réseaux publics contiennent des ressources accessibles depuis Internet. <br>

👉‍ Les attributs communs pour les instances dans les sous-réseaux publics sont : <br>

👉‍ Adresse IP élastique (EIP) ou adresse IP publique attachée à l'instance EC2. <br>

👉‍ IGW attaché au VPC. <br>

👉‍ Le sous-réseau doit avoir une entrée de table de routage avec destination comme passerelle Internet (IGW) 0.0.0.0/0. <br>

👉‍ Les groupes de sécurité et les NACL ne doivent pas bloquer l'accès à distance. <br>

👉‍ Les sous-réseaux publics sont associés à une table de routage qui dirige le trafic du sous-réseau vers Internet à l'aide d'une passerelle Internet. <br>

👉‍ Les sous-réseaux privés contiennent des ressources accessibles depuis le réseau VPC. <br>

👉‍ Plusieurs sous-réseaux peuvent être associés à une seule table de routage. Cependant, un seul sous-réseau ne peut pas être associé à plusieurs tables de routage. <br>

👉‍ Les tables de routage contiennent des ensembles de règles, appelées routes, qui sont utilisées pour déterminer où le trafic est dirigé. <br>

👉‍ Chaque sous-réseau d'un VPC est lié à la table de routage par défaut. <br>

👉‍ Les tables de routage primaires ou principales sont celles qui accompagnent automatiquement votre VPC. Ils contrôlent le routage de tous les sous-réseaux qui ne sont explicitement associés à aucune autre table de routage. <br>

👉‍ La table de routage par défaut ne peut pas être supprimée. <br>

👉‍ Les tables de routage personnalisées sont celles que vous créez pour votre VPC, et vous pouvez ajouter des routes selon vos besoins. <br>

👉‍ Les tables de routage personnalisées peuvent être supprimées lorsqu'elles ne sont pas nécessaires. <br>

👉‍ Internet Gateway (IGW) est un routeur virtuel qui aide un VPC à se connecter à Internet. <br>

👉‍ Par défaut, les instances lancées dans un VPC ne peuvent pas communiquer avec Internet. Pour activer l'accès à Internet, la passerelle Internet devait être attachée au VPC. <br>

👉‍ Les sous-réseaux publics sont connectés à IGW via des tables de routage pour être accessibles via Internet. <br>

👉‍ Les passerelles Internet sont évolutives horizontalement, hautement disponibles et redondantes. <br>

👉‍ EIP, l'adresse IP élastique est une adresse IPv4 statique utilisée par AWS pour gérer ses services de cloud computing dynamique. <br>

👉‍ Il est associé à un compte AWS et vous pouvez l'utiliser pour masquer si une défaillance d'instance se produit, c'est-à-dire si un serveur tombe en panne, nous pouvons mapper cette adresse IP sur un autre serveur et continuer à nous déplacer sans aucun problème.. <br>

👉‍ Les périphériques NAT peuvent être soit une instance/passerelle résidant dans un sous-réseau public (auquel un EIP est attribué). <br>

👉‍ Les appareils NAT aident les instances des sous-réseaux privés à interagir avec Internet. <br>

👉‍ La liste de contrôle d'accès (ACL) est une couche de sécurité facultative qui agit comme un pare-feu pour contrôler le trafic réseau entrant et sortant du sous-réseau.. <br>

👉‍ Les règles sont définies avec l'ACL pour autoriser ou refuser le trafic réseau sur les ports / adresses IP. <br>

👉‍ Vous pouvez attribuer un seul bloc CIDR à un VPC. La taille de bloc autorisée est comprise entre un masque de réseau /28 et un masque de réseau /16. En d'autres termes, le VPC peut contenir de 16 à 65 536 adresses IP <br>
