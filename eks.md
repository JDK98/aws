Elastic Kubernetes Service
---

Il s'agit d'un service Open source de gestion des conteneurs sur AWS <br>
Toutes les technologies natives de kubernetes peuvent être utilisé <br>
Et la migration vers un autre provider est moins complexe  <br>
Dans la majeure partie des cas, il est celui à utiliser pour provisionner  <br>
des clusters kubernetes sur AWS <br>
Il propose trois types de gestions : `Self-managed, Semi-managed, Fargate ` <br>
Quelques soient le type choisi, le master est AWS managé

# Self-managed
Dans cette approche, l'administration des worker nodes provisionnés est à la charge de l'utilisateur  <br>

# Semi-managed
Dans le mode Semi managé, les workers sont créés par groupes managés par AWS <br>

# Fargate
Dans ce mode, l'administration des worker nodes et des conteneurs est totaleent managé par AWS <br>

# Créer un cluster EKS manuellement
[EKS Documentation](https://docs.aws.amazon.com/eks/latest/userguide/create-cluster.html#aws-management-console)
1. Creér un EKS IAM Role  <br>
2. Créer un VPC pour les Worker Nodes (AWS CloudFormation public & privé) <br>
3. Créer le cluster EKS (Master Node) <br>
4. Connection au cluster via kubectl  <br>
`aws eks update-kubeconfig --name [Cluster-name]` <br>
Info à propos du cluster `kubectl cluster-info` <br>
Toutes les commandes kubectl natives fonctionnent  <br>
5. Créer EC2 IAM Role pour les Nodegroup <br>
- Ajouter les polices: `AmazonEKSWorkerNodePolicy`, `AmazonEC2ContainerRegistryReadOnly`, `AmazonEKS_CNI_Policy` <br>
6. Créer les Nodegroup et les lier au cluster <br>
7. Configuration de l'autoscaling  <br>
8. Déploiement d'une application <br>


# Créer un cluster EKS automatiquement avec eksctl 
1. Installer eksctl <br>
[Installer eksctl](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html) <br>

2. Configurer aws credentials <br>
`aws configure`

3. Créer le cluster avec eksctl <br>

```
eksctl create cluster \
--name [cluster name] \
--version [kubernetes version] \
--region [region] \
--nodegroup-name [nodegroup-name] \
--node-type [ec2-instance-type] \
--nodes [number-of-node] \
--nodes-min [min-of-node] \
--nodes-max [max-of-node] \
```
NB: La création du cluster dure 20 à 25min <br>

Il est egalement possible de créer le cluster via un fichier yaml `eksctl create cluster -f [filename]` <br>
```
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig

metadata:
  name: eks-demo # EKS Cluster name
  region: ${AWS_REGION} # Region Code to place EKS Cluster
  version: "1.21"

availabilityZones: ["${AZS[0]}", "${AZS[1]}", "${AZS[2]}"]

vpc:
  cidr: "192.168.0.0/16" # CIDR of VPC for use in EKS Cluster

managedNodeGroups:
  - name: node-group # Name of node group in EKS Cluster
    instanceType: m5.large # Instance type for node group
    desiredCapacity: 3 # The number of worker node in EKS Cluster
    volumeSize: 10  # EBS Volume for worker node (unit: GiB)
    ssh:
      enableSsm: true
    iam:
      withAddonPolicies:
        imageBuilder: true # Add permission for AWS ECR
        # albIngress: true  # Add permission for ALB Ingress
        cloudWatch: true # Add permission for CloudWatch
        autoScaler: true # Add permission Auto Scaling

cloudWatch:
  clusterLogging:
    enableTypes: ["*"]
```

