CloudFormation
---

    CloudFormation est un service fourni par AWS pour concevoir notre propre infrastructure à l'aide de code, 
    c'est-à-dire l'infrastructure en tant que code.

    Actuellement, CloudFormation prend en charge deux langages JSON et YAML. 
    Vous pouvez écrire votre code avec l'un des langages.

    CloudFormation est livré avec d'excellentes fonctionnalités permettant de mettre à jour votre infrastructure 
    quand vous le souhaitez et également d'avoir la possibilité de supprimer la pile au cas où vous n'en auriez pas besoin.

    Une caractéristique fascinante de cloudFormation est qu'elle permet de gagner plus de temps dans 
    la construction de l'infrastructure et aide à se concentrer sur le développement.

    Il est également possible de répliquer notre infrastructure en peu de temps.

    Il élimine les erreurs humaines et fonctionne selon le code que vous avez écrit. 
    Il se compose de deux composants principaux, Stack et Templates.

# CloudFormation Template

CloudFormation est un service fourni par AWS pour concevoir notre propre infrastructure à l'aide de code, c'est-à-dire l'infrastructure en tant que code.

Il se compose de différentes sections comme

    Version du format de modèle AWS

    La description

    Métadonnées

    Paramètres

    Mappages

    Conditions

    Ressources (champ obligatoire)

    Les sorties

Il n'est pas obligatoire que le modèle requière toutes les sections susmentionnées. 
En utilisant uniquement la section Ressources, nous pourrons créer un modèle. <br>

La section des ressources joue un rôle important dans la création du modèle. <br>

Par exemple, pour créer une instance EC2, un modèle doit être composé de divers paramètres 
tels que le nom de la clé, l'identifiant de l'image, le type d'instance. <br>

Il est également possible de créer deux ressources dans le même modèle et de se référer l'une à l'autre, 
c'est-à-dire d'attacher une adresse IP élastique à une instance EC2. <br>

# CloudFormation Stack

Une pile (Stack) est constituée d'un ensemble de ressources. <br>

En d'autres termes, la pile est constituée d'un ou plusieurs modèles. <br>

L'avantage de la pile est qu'il est facile de créer, supprimer ou mettre à jour la collection de ressources. <br>

Les piles avancées ont une pile imbriquée qui contient une collection de piles. <br>
